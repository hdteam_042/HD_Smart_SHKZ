/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.marshalchen.common.uimodule.easing;


public enum  Skill {

    BackEaseIn(com.marshalchen.common.uimodule.easing.back.BackEaseIn.class),
    BackEaseOut(com.marshalchen.common.uimodule.easing.back.BackEaseOut.class),
    BackEaseInOut(com.marshalchen.common.uimodule.easing.back.BackEaseInOut.class),

    BounceEaseIn(com.marshalchen.common.uimodule.easing.bounce.BounceEaseIn.class),
    BounceEaseOut(com.marshalchen.common.uimodule.easing.bounce.BounceEaseOut.class),
    BounceEaseInOut(com.marshalchen.common.uimodule.easing.bounce.BounceEaseInOut.class),

    CircEaseIn(com.marshalchen.common.uimodule.easing.circ.CircEaseIn.class),
    CircEaseOut(com.marshalchen.common.uimodule.easing.circ.CircEaseOut.class),
    CircEaseInOut(com.marshalchen.common.uimodule.easing.circ.CircEaseInOut.class),

    CubicEaseIn(com.marshalchen.common.uimodule.easing.cubic.CubicEaseIn.class),
    CubicEaseOut(com.marshalchen.common.uimodule.easing.cubic.CubicEaseOut.class),
    CubicEaseInOut(com.marshalchen.common.uimodule.easing.cubic.CubicEaseInOut.class),

    ElasticEaseIn(com.marshalchen.common.uimodule.easing.elastic.ElasticEaseIn.class),
    ElasticEaseOut(com.marshalchen.common.uimodule.easing.elastic.ElasticEaseOut.class),

    ExpoEaseIn(com.marshalchen.common.uimodule.easing.expo.ExpoEaseIn.class),
    ExpoEaseOut(com.marshalchen.common.uimodule.easing.expo.ExpoEaseOut.class),
    ExpoEaseInOut(com.marshalchen.common.uimodule.easing.expo.ExpoEaseInOut.class),

    QuadEaseIn(com.marshalchen.common.uimodule.easing.quad.QuadEaseIn.class),
    QuadEaseOut(com.marshalchen.common.uimodule.easing.quad.QuadEaseOut.class),
    QuadEaseInOut(com.marshalchen.common.uimodule.easing.quad.QuadEaseInOut.class),

    QuintEaseIn(com.marshalchen.common.uimodule.easing.quint.QuintEaseIn.class),
    QuintEaseOut(com.marshalchen.common.uimodule.easing.quint.QuintEaseOut.class),
    QuintEaseInOut(com.marshalchen.common.uimodule.easing.quint.QuintEaseInOut.class),

    SineEaseIn(com.marshalchen.common.uimodule.easing.sine.SineEaseIn.class),
    SineEaseOut(com.marshalchen.common.uimodule.easing.sine.SineEaseOut.class),
    SineEaseInOut(com.marshalchen.common.uimodule.easing.sine.SineEaseInOut.class),

    Linear(com.marshalchen.common.uimodule.easing.linear.Linear.class);


    private Class easingMethod;

    private Skill(Class clazz) {
        easingMethod = clazz;
    }

    public BaseEasingMethod getMethod(float duration) {
        try {
            return (BaseEasingMethod)easingMethod.getConstructor(float.class).newInstance(duration);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Can not init easingMethod instance");
        }
    }
}
