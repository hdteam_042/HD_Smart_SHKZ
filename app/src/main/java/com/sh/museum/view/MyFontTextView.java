package com.sh.museum.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sh.museum.base.HD_Application;


public class MyFontTextView extends TextView {
    public MyFontTextView(Context context) {
        super(context);
        setTypeface();
    }

    public MyFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface();
    }

    public MyFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface();
    }

    private void setTypeface() {
        if (HD_Application.TEXT_TYPE == null) {
            setTypeface(getTypeface());
        } else {
            setTypeface(HD_Application.TEXT_TYPE);
        }
    }
}  