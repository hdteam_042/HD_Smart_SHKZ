package com.sh.museum.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.widget.Toast;


import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sh.museum.R;
import com.sh.museum.common.Constants;
import com.sh.museum.entities.VersionInfo;

import java.io.File;

/**
 * @author：JFWU on 2016/8/1 14:37
 * @E-mail：jeffy12138@126.com
 */
public class VersionDownLoadDialog extends ProgressDialog {
    private Context mContext;
    private String TEMP_DIR;
    private String DOWNLOAD_DIR;
    private Handler handler;
    private VersionInfo mInfo;
    private static HttpHandler<File> httpHandler;
    private static com.lidroid.xutils.HttpUtils utils;
    private int progress;

    public VersionDownLoadDialog(Context context, Handler handler, VersionInfo info) {
        super(context);
        mContext = context;
        this.handler = handler;
        mInfo = info;
        utils = new HttpUtils();
    }

    public VersionDownLoadDialog(Context context, int theme, Handler handler, VersionInfo info) {
        super(context, theme);
        mContext = context;
        this.handler = handler;
        mInfo = info;
        utils = new HttpUtils();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final File file = new File(Constants.BASEPATH + "temp/", SystemClock.uptimeMillis() + ".apk");
        DOWNLOAD_DIR = mInfo.getVersionInfo().getVersionUrl();
        TEMP_DIR = Constants.BASEPATH + "temp/";
        httpHandler = utils.download(DOWNLOAD_DIR, file.getAbsolutePath(), new RequestCallBack<File>() {
            @Override
            public void onSuccess(ResponseInfo<File> responseInfo) {
                dismiss();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.download_complete), Toast.LENGTH_LONG).show();
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(
                        Uri.parse("file://" + file.getAbsolutePath()),
                        "application/vnd.android.package-archive");
                installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(installIntent);
                handler.sendEmptyMessage(Constants.EXIT);
            }

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                super.onLoading(total, current, isUploading);
                progress = (int) ((float) current / (float) total * 100);
                VersionDownLoadDialog.this.setProgress(progress);
            }

            @Override
            public void onFailure(HttpException e, String s) {
                dismiss();
                Toast.makeText(mContext, mContext.getResources().getString(R.string.download_failed), Toast.LENGTH_LONG).show();
                handler.sendEmptyMessage(1);

            }
        });

        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
                    httpHandler.cancel();
                    VersionDownLoadDialog.this.dismiss();
                    handler.sendEmptyMessage(1);
                }
                return false;
            }
        });
    }
}
