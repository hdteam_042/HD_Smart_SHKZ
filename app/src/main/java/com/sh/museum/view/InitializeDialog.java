package com.sh.museum.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;


import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import com.sh.museum.R;
import com.sh.museum.utils.FileUtils;


import java.io.File;

/**
 * @author：JFWU on 2016/7/30 13:22
 * @E-mail：jeffy12138@126.com
 */
public class InitializeDialog extends ProgressDialog {
    private Context mContext;
    private String TEMP_DIR;
    private String DOWNLOAD_DIR;
    private String UNZIP_DIR;
    private Handler mHandler;
    private HttpUtils http = new HttpUtils();
    private HttpHandler handler;
    public InitializeDialog(Context context, Handler handler, String TEMP_DIR, String DOWNLOAD_DIR, String UNZIP_DIR) {
        super(context);
        mContext = context;
        this.mHandler = handler;
        this.DOWNLOAD_DIR = DOWNLOAD_DIR;
        this.TEMP_DIR = TEMP_DIR;
        this.UNZIP_DIR = UNZIP_DIR;
    }

    public InitializeDialog(Context context, int theme, Handler handler, String TEMP_DIR, String DOWNLOAD_DIR, String UNZIP_DIR) {
        super(context, theme);
        mContext = context;
        this.mHandler = handler;
        this.DOWNLOAD_DIR = DOWNLOAD_DIR;
        this.TEMP_DIR = TEMP_DIR;
        this.UNZIP_DIR = UNZIP_DIR;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = http.download(DOWNLOAD_DIR, TEMP_DIR, false, false,
                new RequestCallBack<File>() {

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onLoading(long total, long current, boolean isUploading) {

                    }

                    @Override
                    public void onSuccess(ResponseInfo<File> responseInfo) {
                        InitializeDialog.this.dismiss();
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                FileUtils.unZip(TEMP_DIR,UNZIP_DIR);
                                InitializeDialog.this.dismiss();
                                mHandler.sendEmptyMessage(0);
                                File delFile = new File(TEMP_DIR);
                                if (delFile.exists()) {
                                    delFile.delete();
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(HttpException error, String msg) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.ini_failed), Toast.LENGTH_LONG).show();
                        mHandler.sendEmptyMessage(10);
                        InitializeDialog.this.dismiss();
                    }
                }

        );

        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getRepeatCount() == 0) {
                    handler.cancel();
                    InitializeDialog.this.dismiss();
                    mHandler.sendEmptyMessage(102);
                }
                return false;
            }
        });
    }
}
