package com.sh.museum.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.sh.museum.base.HD_Application;


public class MyFontEditText extends EditText {
    public MyFontEditText(Context context) {
        super(context);
        setTypeface();
    }

    public MyFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface();
    }

    public MyFontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface();
    }

    private void setTypeface() {
        if (HD_Application.TEXT_TYPE == null) {
            setTypeface(getTypeface());
        } else {
            setTypeface(HD_Application.TEXT_TYPE);
        }
    }
}  