package com.sh.museum.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ResourceDb {
    private final static String FIELD_PLAYNUMS = "AutoNum";
    private final static String FIELD_FILENO = "FileNo";
    private final static String FIELD_TYPENUM = "TypeNo";
    private String CurDBFileName;
    private SQLiteDatabase db = null;
    private Cursor c = null;
    public volatile static ResourceDb _instance;

    private ResourceDb() {
    }

    public static ResourceDb get_instance() {
        if (_instance == null)
            synchronized (ResourceDb.class) {
                if (_instance == null) {
                    _instance = new ResourceDb();
                }
            }
        return _instance;
    }


    public boolean OpenDB(String DBPathFile) {
        CurDBFileName = DBPathFile;
        if (db != null) {
            db.close();
        }
        if (c != null) {
            c.close();
        }
        try {
            db = SQLiteDatabase.openDatabase(DBPathFile, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            return true;
        } catch (Exception E) {
            db = null;
            return false;
        }
    }

    public void CloseDB() {
        if (db != null) {
            db.close();
            db = null;
        }
        if (c != null) {
            c.close();
            c = null;
        }
    }

    public Cursor getUnitList(String CurLang) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang, null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor getBoutiqueList(String CurLang) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang, null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor resourceList(String CurLang, String UnitNo) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang + " WHERE "
                        + FIELD_TYPENUM + " = " + UnitNo, null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor digitalList(String CurLang) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang, null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor PlayByAutoNum(String CurLang, int UnitNo) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            db.beginTransaction();
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang + " WHERE "
                        + FIELD_PLAYNUMS + " = " + "'" + UnitNo + "'", null);
                db.setTransactionSuccessful();
            } catch (Exception E) {
                c = null;
            } finally {
                db.endTransaction();
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor PlayByFileNum(String CurLang, String UnitNo) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT * FROM " + CurLang + " WHERE "
                        + FIELD_FILENO + " = " + "'" + UnitNo + "'", null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor getDistanceByAutoNum(String CurLang, int autoNum) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery("SELECT distance FROM " + CurLang + " WHERE "+ FIELD_PLAYNUMS + " = " + "'" + autoNum + "'", null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }

    public Cursor locate(String CurLang, int autoNum) {
        return Base(CurLang, "SELECT  X,Y,Floor FROM " +CurLang+ " WHERE  AutoNum" + " = " + autoNum);
    }

    public Cursor getBase(String CurLang) {
        return Base(CurLang, "SELECT * FROM " + CurLang);
    }

    public Cursor getInfo(String CurLang, int floor) {
        return Base(CurLang, "SELECT * FROM " + CurLang + " WHERE  Floor" + " = " + floor);
    }

    public Cursor Base(String CurLang, String sqlSentence) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {
                c = db.rawQuery(sqlSentence, null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }
    public Cursor upDateByAutoNum(String CurLang, int autoNum,double distance ) {
        if (db == null) {
            try {
                db = SQLiteDatabase.openDatabase(CurDBFileName, null,
                        SQLiteDatabase.OPEN_READWRITE
                                | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            } catch (Exception E) {
                db = null;
            }
        }
        if (db != null) {
            if (c != null) {
                c.close();
                c = null;
            }
            try {//UPDATE Person SET FirstName = 'Fred' WHERE LastName = 'Wilson'
                c = db.rawQuery("UPDATE " + CurLang + " SET distance ="+ "'" + distance + "'" + " WHERE "+ FIELD_PLAYNUMS + " = " + "'" + autoNum + "'", null);
            } catch (Exception E) {
                c = null;
            }
        }
        if (c != null) {
            if (c.getCount() <= 0) {
                c.close();
                c = null;
            }
        }
        return c;
    }
}
