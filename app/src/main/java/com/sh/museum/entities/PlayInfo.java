package com.sh.museum.entities;

import java.io.Serializable;

/**
 * Created by baishiwei on 2015/1/13.
 */
public class PlayInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String path;
    private String info;
    private String num;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "PlayInfo{" +
                "path='" + path + '\'' +
                ", info='" + info + '\'' +
                ", num='" + num + '\'' +
                '}';
    }
}
