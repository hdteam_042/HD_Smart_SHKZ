package com.sh.museum.entities;

import java.io.Serializable;

/**
 * Created by baishiwei on 2015/5/7.
 */
public class MapBase implements Serializable {
    private static final long serialVersionUID = 1L;
    private double width;
    private double height;
    private int scale;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "MapBase{" +
                "width=" + width +
                ", height=" + height +
                ", scale=" + scale +
                '}';
    }
}
