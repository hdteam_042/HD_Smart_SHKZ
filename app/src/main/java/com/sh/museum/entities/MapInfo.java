package com.sh.museum.entities;

import java.io.Serializable;

/**
 * Created by baishiwei on 2015/5/7.
 */
public class MapInfo implements Serializable {
    private static final long serialVersionUID = 2L;
    private String num;
    private double x;
    private double y;
    private String info;
    private int floor;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "MapInfo{" +
                "num='" + num + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", info='" + info + '\'' +
                ", floor=" + floor +
                '}';
    }
}
