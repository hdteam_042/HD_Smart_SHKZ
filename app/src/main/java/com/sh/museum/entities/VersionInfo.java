package com.sh.museum.entities;

/**
 * @author：JFWU on 2016/8/19 11:52
 * @E-mail：jeffy12138@126.com
 */
public class VersionInfo {

    /**
     * status : 2002
     * msg : 有新的版本
     * versionInfo : {"versionNo":"5","versionName":"1.0.4","versionUrl":"http://hengdawb-update.oss-cn-hangzhou.aliyuncs.com/TaiZhou/TaiZhou_v1.0.4.apk","versionLog":"此版本已过期，为保证功能的使用，请升级成最新版本!"}
     */

    private String status;
    private String msg;
    /**
     * versionNo : 5
     * versionName : 1.0.4
     * versionUrl : http://hengdawb-update.oss-cn-hangzhou.aliyuncs.com/TaiZhou/TaiZhou_v1.0.4.apk
     * versionLog : 此版本已过期，为保证功能的使用，请升级成最新版本!
     */

    private VersionInfoBean versionInfo;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public VersionInfoBean getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(VersionInfoBean versionInfo) {
        this.versionInfo = versionInfo;
    }

    public static class VersionInfoBean {
        private String versionNo;
        private String versionName;
        private String versionUrl;
        private String versionLog;

        public String getVersionNo() {
            return versionNo;
        }

        public void setVersionNo(String versionNo) {
            this.versionNo = versionNo;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getVersionUrl() {
            return versionUrl;
        }

        public void setVersionUrl(String versionUrl) {
            this.versionUrl = versionUrl;
        }

        public String getVersionLog() {
            return versionLog;
        }

        public void setVersionLog(String versionLog) {
            this.versionLog = versionLog;
        }
    }
}
