package com.sh.museum.base;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Typeface;

import com.sh.museum.utils.NetworkConnectChangedReceiver;

public class HD_Application extends android.app.Application {
    public static Typeface TEXT_TYPE = null;
    public static Context mContext;

    @Override
    public void onCreate() {
        mContext = getApplicationContext();
        try {
            TEXT_TYPE = Typeface.createFromAsset(getAssets(), "fonts/jlx.TTF");
        } catch (Exception e) {
            TEXT_TYPE = null;
        }
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(new NetworkConnectChangedReceiver(), filter);
    }
}
