package com.sh.museum.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.sh.museum.common.Constants;
import com.sh.museum.utils.SharedPrefUtil;


/**
 * @author baishiwei
 * @date:2014-7-25
 * @time:下午2:24:09
 */
public class BaseFragmentActivity extends FragmentActivity {
    public SharedPrefUtil sp;
    protected AlertDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        sp = new SharedPrefUtil(getApplicationContext(), Constants.SHARE_NAME);
    }


    protected void showShortToast(int pResId) {
        showShortToast(getString(pResId));
    }


    protected void showShortToast(String pMsg) {
        Toast.makeText(this, pMsg, Toast.LENGTH_SHORT).show();
    }


    public void defaultFinish() {
        super.finish();
    }

    protected void openActivity(Class<?> pClass) {
        openActivity(pClass, null);
    }

    protected void openActivity(Class<?> pClass, Bundle pBundle) {
        Intent intent = new Intent(this, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
    }
    protected ProgressDialog showProgressDialog(String pTitle, String pMessage, DialogInterface.OnCancelListener pCancelClickListener) {
        progressDialog = ProgressDialog.show(this, null, pMessage, true, false);
        progressDialog.setOnCancelListener(pCancelClickListener);
        return (ProgressDialog) progressDialog;
    }

    protected void dismissDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}