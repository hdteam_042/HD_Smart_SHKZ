package com.sh.museum.utils;

import com.sh.museum.base.HD_Application;
import com.sh.museum.common.Constants;

/**
 * Created by lenovo on 2016/12/16.
 */

public class HdAppConfig {
    private static SharedPrefUtil appConfigShare = new SharedPrefUtil(HD_Application.mContext,
            Constants.SHARED_PREF_NAME);
    public static final String ADDRESS = "ADDRESS";

    public static void setAddress(String address){
        appConfigShare.setPrefString(ADDRESS,address);
    }

    public static String getAddress(){
        return appConfigShare.getPrefString(ADDRESS);
    }
}
