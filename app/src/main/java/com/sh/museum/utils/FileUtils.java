package com.sh.museum.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.sh.museum.common.Constants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {

    public static void unZip(String unZipfileName, String mDestPath) {
        if (!mDestPath.endsWith("/")) {
            mDestPath = mDestPath + "/";
        }
        FileOutputStream fileOut = null;
        ZipInputStream zipIn = null;
        ZipEntry zipEntry = null;
        File file = null;
        int readedBytes = 0;
        byte buf[] = new byte[4096];
        try {
            zipIn = new ZipInputStream(new BufferedInputStream(
                    new FileInputStream(unZipfileName)));
            while ((zipEntry = zipIn.getNextEntry()) != null) {
                file = new File(mDestPath + zipEntry.getName());
                if (zipEntry.isDirectory()) {
                    file.mkdirs();
                } else {
                    File parent = file.getParentFile();
                    if (!parent.exists()) {
                        parent.mkdirs();
                    }
                    fileOut = new FileOutputStream(file);
                    while ((readedBytes = zipIn.read(buf)) > 0) {
                        fileOut.write(buf, 0, readedBytes);
                    }
                    fileOut.close();
                }
                zipIn.closeEntry();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
    }

    public static void LoadResource(Context context, String fileName, String filePath, int IDNo) {
        LoadFile(context, Constants.BASEPATH + "temp/", Constants.BASEPATH + "temp/"
                + fileName + ".zip", IDNo);
        unZip(Constants.BASEPATH + "temp/" + fileName + ".zip", Constants.BASEPATH + filePath + "/");
        File delFile = new File(Constants.BASEPATH + "temp/" + fileName + ".zip");
        if (delFile.exists()) {
            delFile.delete();
        }

    }

    private static void LoadFile(Context context, String TargetFilePath, String TargetFileName, int file_id) {
        try {
            File dir = new File(TargetFilePath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            if (!(new File(TargetFileName)).exists()) {
                InputStream is = context.getResources()
                        .openRawResource(file_id);
                FileOutputStream fos = new FileOutputStream(TargetFileName);
                byte[] buffer = new byte[8192];
                int count = 0;

                while ((count = is.read(buffer)) > 0) {
                    fos.write(buffer, 0, count);
                }

                fos.close();
                is.close();
            }
        } catch (Exception e) {

        }
    }

    /**
     * �����ļ�����·����ȡ�ļ���
     *
     * @param filePath
     * @return
     */
    public static String getFileName(String filePath) {
        if (isEmpty(filePath))
            return "";
        return filePath.substring(filePath.lastIndexOf(File.separator) + 1);
    }

    public static boolean isEmpty(String input) {
        if (input == null || "".equals(input))
            return true;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;
    }

    public static boolean isFileExist(String centerPath, String num, String Type) {
        boolean flag;
        String path = Constants.BASEPATH + centerPath + "/" + num + "." + Type.toLowerCase();
        File temp_file = new File(path);
        if (temp_file.exists()) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    public static int getCurrentVersion(Context context) {
        int curVersionCode = 0;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            curVersionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        return curVersionCode;
    }
    public static String getVersionName(Context context) {
        String curVersionName ="1.0";
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            curVersionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        return curVersionName;
    }
}
