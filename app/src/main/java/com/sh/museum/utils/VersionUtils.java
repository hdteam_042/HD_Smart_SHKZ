package com.sh.museum.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.sh.museum.R;
import com.sh.museum.common.Constants;
import com.sh.museum.entities.VersionInfo;
import com.sh.museum.view.VersionDownLoadDialog;


/**
 * Created by Administrator on 2016/7/4.
 */
public class VersionUtils {
    private static HttpUtils utils;
    private static VersionDownLoadDialog dialog;


    public static void getVersion(final Context context, final Handler mHandler) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        utils = new HttpUtils();
        RequestParams params = new RequestParams();
        params.addBodyParameter("appKey", "7fb674619973ebcfd81a87aae020706e");
        params.addBodyParameter("appKind", "1");
        params.addBodyParameter("deviceId", deviceId);
        params.addBodyParameter("versionCode", FileUtils.getCurrentVersion(context) + "");
        params.addBodyParameter("appSecret", "26997567cab5bea222e01b1228615c22");
        String url = "http://101.200.234.14/APPCloud/index.php?a=checkVersion";
        utils.send(HttpRequest.HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {

                System.out.println("版本信息：" + responseInfo.result);
                Gson gson = new Gson();
                VersionInfo VersionInfo = gson.fromJson(responseInfo.result, VersionInfo.class);
                switch (VersionInfo.getStatus()) {
                    case "2001":
                        mHandler.sendEmptyMessage(1);
                        break;
                    case "2002":
                        showUpdateDialog(VersionInfo, context, mHandler);
                        break;
                    case "4041":
                        mHandler.sendEmptyMessage(1);
                        break;
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                mHandler.sendEmptyMessage(1);

            }
        });


    }

    private static void showUpdateDialog(final VersionInfo VersionInfo, final Context context, final Handler mHandler) {
        final NiftyDialogBuilder builder1 = new NiftyDialogBuilder(context, R.style.dialog_untran);
        Effectstype effect1 = Effectstype.Slidetop;
        builder1.isCancelableOnTouchOutside(false);
        builder1.withTitle(context.getResources().getString(R.string.tips))
                .withMessage(VersionInfo.getVersionInfo().getVersionLog())
                .withIcon(context.getResources().getDrawable(R.drawable.app_icon))
                .withDuration(700)
                .withEffect(effect1)
                .withButton1Text(context.getResources().getString(R.string.confirm)).setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new VersionDownLoadDialog(context, mHandler, VersionInfo);
                dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dialog.setIcon(R.drawable.app_icon);
                dialog.setTitle("版本更新中...");
                dialog.setCancelable(false);
                dialog.show();
                builder1.dismiss();
            }
        }).withButton2Text(context.getResources().getString(R.string.cancel)).setButton2Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(1);
                builder1.dismiss();
            }
        });
        builder1.show();
    }

}
