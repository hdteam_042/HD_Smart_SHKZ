package com.sh.museum.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import java.io.File;
import java.util.Locale;


public class Utils {

    public static void configLanguage(Context context, String language) {
        Configuration config = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (language.equals("CHINESE")) {
                config.locale = Locale.SIMPLIFIED_CHINESE;
            } else if (language.equals("ENGLISH")) {
                config.locale = Locale.US;
            } else if (language.equals("JAPANESE")) {
                config.locale = Locale.JAPAN;
            }
        } else {
            if (language.equals("CHINESE")) {
                config.locale = Locale.CHINESE;
            } else if (language.equals("ENGLISH")) {
                config.locale = Locale.ENGLISH;
            } else if (language.equals("JAPANESE")) {
                config.locale = Locale.JAPAN;
            }
        }
        context.getResources().updateConfiguration(config, null);
    }

    public static boolean isFileExist(String path) {
        boolean flag;
        File file = new File(path);
        if (file.exists()) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;

    }
    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }
    public static void deleteDir(String path) {
        File dir = new File(path);
        if (dir == null || !dir.exists() || !dir.isDirectory())
            return;
        for (File file : dir.listFiles()) {
            if (file.isFile())
                file.delete();
            else if (file.isDirectory())
                deleteDir(file.getPath());
        }
        dir.delete();
    }
}
