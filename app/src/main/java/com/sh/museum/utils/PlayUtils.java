package com.sh.museum.utils;

import android.database.Cursor;

import com.sh.museum.entities.PlayInfo;
import com.sh.museum.common.Constants;

import java.util.Map;

/**
 * Created by baishiwei on 2015/1/23.
 */
public class PlayUtils {
    public static PlayInfo setPlayInfo(Cursor cursor) {
        PlayInfo info = new PlayInfo();
        info.setPath(Constants.BASEPATH + cursor.getString(2).toString() + "/" + cursor.getString(1).toString() + "." + cursor.getString(3).toLowerCase().toString());
        info.setInfo(cursor.getString(4).toString());
        info.setNum(cursor.getString(1).toString());
        return info;
    }

    public static PlayInfo setPlayInfo(Map<String, Object> data) {
        PlayInfo info = new PlayInfo();
        info.setPath(Constants.BASEPATH + data.get("Path") + "/" + data.get("Num") + "." + data.get("Type").toString().toLowerCase());
        info.setInfo(data.get("title").toString());
        info.setNum(data.get("Num").toString());
        return info;
    }
    public static PlayInfo setImageInfo(Map<String, Object> data) {
        PlayInfo info = new PlayInfo();
        info.setPath(Constants.BASEPATH + data.get("Path") + "/" + data.get("Num") + ".png");
        info.setInfo(data.get("title").toString());
        info.setNum(data.get("Num").toString());
        return info;
    }
}
