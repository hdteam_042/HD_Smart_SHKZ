package com.sh.museum.common;

public class Constants {
    public static final String SHARED_PREF_NAME = "HD_SHKZJNG_PREF";
    public static final String ADDERSS_INNER = "http://192.168.16.30/12345/HD_SH_RES/";
    public static final String ADDERSS_OUTTER = "http://hengdawb-res.oss-cn-hangzhou.aliyuncs.com/SongHu_SmartGuide/";
    public static String ADDRESS ;
    public static final String SSID1 = "SHKZJNG";
    public static final String SSID2 = "HD_TESTTEST";
    public static String Cookie_Config = "27.112.2.37";

    public final static String BASEPATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/SongHu_Res/";
    public static final String DATABASE_PATH = BASEPATH + "/database/Filemanage.s3db";

    public static boolean PLAYER_FLAG = false;
    public static boolean DOWNLOAD_FLAG = false;
    public static String LANGUAGE = "CHINESE";
    public static final String SHARE_NAME = "HENGDA";
    public static final String PLAY_INTENT = "tianjinst.play.ready.info";
    public static final String MAP_LOCATION = "tianjinst.map.location";
    public static final String MAP_NAVIGATION = "tianjinst.map.navigation";


    public static final int DOWNLOAD_FAIL = 100;
    public static final int UNZIP_FINISH = 101;
    public static final int EXIT = 102;
    public static final int NO_NETWORK = 103;
    public static final int DELETE_FINISH = 104;

}
