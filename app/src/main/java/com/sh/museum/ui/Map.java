package com.sh.museum.ui;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.base.BaseFragmentActivity;
import com.sh.museum.common.Constants;
import com.sh.museum.db.ResourceDb;
import com.sh.museum.entities.MapBase;
import com.sh.museum.fragment.MapFragment;
import com.sh.museum.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ContentView(R.layout.map)
public class Map extends BaseFragmentActivity {
    @ViewInject(R.id.top_back_btn)
    ImageButton back;
    @ViewInject(R.id.top_title)
    TextView title;
    private String language = Constants.LANGUAGE;
    private List<java.util.Map<String, Object>> data = new ArrayList<java.util.Map<String, Object>>();
    private MapBase baseInfo = new MapBase();
    private MapBase baseInfo1 = new MapBase();
    private FragmentManager fm;
    private int temp_floor = 2;
    private Intent intent_location, intent_navigation;
    @ViewInject(R.id.top_floor_select_btn)
    private Button floor_btn;
    @ViewInject(R.id.road)
    private ImageButton road;
    private PopupWindow pop;
    private String[] floorArr;
    private ListView list;
    private boolean isContinue = true;
    private String map_path;
    private boolean b = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        map_path = Constants.BASEPATH + language + "/map/";
        File file = new File(map_path);
        if (file.exists()) {
            getBaseInfo();
            getBaseInfo1();
            iniPopData();
            initPop();
            fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.map_fragment, MapFragment.newInstance(baseInfo, temp_floor, language, false)).commit();
            intent_location = new Intent(Constants.MAP_LOCATION);
            intent_navigation = new Intent(Constants.MAP_NAVIGATION);
        } else {
            Toast.makeText(Map.this, getResources().getString(R.string.please_download_resource), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Map.this, Settings.class);
            startActivity(intent);
            finish();
        }


    }

    @OnClick(R.id.top_back_btn)
    public void back(View view) {
        defaultFinish();
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    fm.beginTransaction().replace(R.id.map_fragment, MapFragment.newInstance(baseInfo, temp_floor, language, false)).commit();
                    dismissDialog();
                    sendEmptyMessageDelayed(1, 2000);
                    break;
                case 1:
                    isContinue = true;
                    break;
            }
        }
    };

    private void getBaseInfo() {
        Cursor cursor = ResourceDb.get_instance().getBase("map_base");
        if (cursor != null) {
            cursor.moveToFirst();
            baseInfo.setWidth(cursor.getDouble(0));
            baseInfo.setHeight(cursor.getDouble(1));
            baseInfo.setScale(cursor.getInt(2));
        }
    }

    private void getBaseInfo1() {
        Cursor cursor = ResourceDb.get_instance().getBase("map_base1");
        if (cursor != null) {
            cursor.moveToFirst();
            baseInfo1.setWidth(cursor.getDouble(0));
            baseInfo1.setHeight(cursor.getDouble(1));
            baseInfo1.setScale(cursor.getInt(2));
        }
    }

    @OnClick(R.id.road)
    public void road(View view) {
//        if (language.equals("CHINESE")) {
//
//            if (b) {
//                fm.beginTransaction().replace(R.id.map_fragment, MapFragment.newInstance(baseInfo1, temp_floor, language, b)).commit();
//                b = false;
//            } else {
//                fm.beginTransaction().replace(R.id.map_fragment, MapFragment.newInstance(baseInfo, temp_floor, language, false)).commit();
//                b = true;
//            }
//
//
//        } else {
//            sendBroadcast(intent_navigation);
//        }

        sendBroadcast(intent_navigation);
    }

    @OnClick(R.id.top_floor_select_btn)
    public void floor_btn(View view) {
        if (pop.isShowing()) {
            Drawable drawable = getResources().getDrawable(R.drawable.map_floor_selector_down);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            floor_btn.setCompoundDrawables(null, null, drawable, null);
            pop.dismiss();
        } else {
            pop.showAsDropDown(floor_btn, -10, 0);
            Drawable drawable = getResources().getDrawable(R.drawable.map_floor_selector_down);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            floor_btn.setCompoundDrawables(null, null, drawable, null);
        }
    }

    private void iniPopData() {
        floorArr = getResources().getStringArray(R.array.floor);
        floor_btn.setText(floorArr[1]);
        java.util.Map<String, Object> map;
        for (int i = 0; i < floorArr.length; i++) {
            map = new HashMap<String, Object>();
            map.put("floor", floorArr[i]);
            data.add(map);
        }
    }

    private void initPop() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.map_select_layout, null);
        list = (ListView) layout.findViewById(R.id.pop_list);
        pop = new PopupWindow(layout);
        list.setAdapter(new SimpleAdapter(Map.this, data, R.layout.map_list_item, new String[]{"floor"}, new int[]{R.id.text}));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                floor_btn.setText(floorArr[position]);
                Drawable drawable = getResources().getDrawable(R.drawable.map_floor_selector_down);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                floor_btn.setCompoundDrawables(null, null, drawable, null);
                toggleMap(position + 1);
                pop.dismiss();
            }
        });
        list.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        pop.setWidth(list.getMeasuredWidth());
        pop.setHeight(Utils.dip2px(Map.this, 160));
        //  pop.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.popupwindow_bg));
    }

    private void toggleMap(int floor) {
        if (mHandler.hasMessages(1))
            mHandler.removeMessages(1);
        isContinue = false;
        showProgressDialog(null, getString(R.string.map_loading), null);
        temp_floor = floor;
        mHandler.sendEmptyMessageDelayed(0, 3000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHandler.hasMessages(1))
            mHandler.removeMessages(1);
        if (mHandler.hasMessages(0))
            mHandler.removeMessages(0);
    }
}
