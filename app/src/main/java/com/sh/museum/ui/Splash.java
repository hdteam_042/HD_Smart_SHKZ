package com.sh.museum.ui;

import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.sh.museum.R;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.common.Constants;
import com.sh.museum.utils.VersionUtils;

public class Splash extends BaseActivity {
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    openActivity(Language.class);
                    finish();
                    break;
                case Constants.UNZIP_FINISH:
                    openActivity(Language.class);
                    finish();
                    break;

                case Constants.EXIT:
                    finish();
                    //System.exit(0);
                    break;
                case Constants.NO_NETWORK:
                    showNetWorkDialog();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = View.inflate(this, R.layout.splash, null);
        setContentView(view);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                if (getConnectedType(Splash.this) == 1 && (getWifiName().contains(Constants.SSID1)||getWifiName().contains(Constants.SSID2))) {
                    openActivity(Language.class);
                    finish();
                } else {
                    VersionUtils.getVersion(Splash.this, mHandler);

                }
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        openActivity(Language.class);
//                        finish();
//                    }
//                }, 500);
            }
        });
    }

    private String getWifiName() {
        String wifiName;
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        wifiName = wifiInfo.getSSID();
        return wifiName;
    }

    public void showNetWorkDialog() {
        final NiftyDialogBuilder builder = new NiftyDialogBuilder(Splash.this, R.style.dialog_untran);
        Effectstype effect = Effectstype.Slidetop;
        builder.withTitle(getString(R.string.network_error))
                .withMessage(getString(R.string.check_network))
                .withIcon(getResources().getDrawable(R.drawable.app_icon))
                .withDuration(700)
                .withEffect(effect)
                .withButton1Text(getString(R.string.confirm)).setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(android.provider.Settings.ACTION_SETTINGS);
                startActivity(in);
                builder.dismiss();
            }
        }).withButton2Text(getString(R.string.cancel)).setButton2Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        builder.show();
    }
}
