package com.sh.museum.ui;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.base.HD_Application;
import com.sh.museum.common.Constants;
import com.sh.museum.db.ResourceDb;
import com.sh.museum.entities.PlayInfo;
import com.sh.museum.utils.FileUtils;
import com.sh.museum.utils.HdAppConfig;
import com.sh.museum.utils.PlayUtils;
import com.sh.museum.utils.Utils;

import java.io.File;
import java.util.ArrayList;

@ContentView(R.layout.main)
public class MainActivity extends BaseActivity {
    @ViewInject(R.id.overview)
    private ImageButton overview;
    @ViewInject(R.id.map)
    private ImageButton map;
    @ViewInject(R.id.digital)
    private ImageButton digital;
    @ViewInject(R.id.qr)
    private ImageButton qr;
    @ViewInject(R.id.auto)
    private ImageButton auto;
    @ViewInject(R.id.service)
    private ImageButton service;
    private String language = Constants.LANGUAGE;
    private long mExitTime;
    private final int GET_CODE = 0;
    private PlayInfo info;
    private Cursor cursor;
    private ArrayList<java.util.Map<String, Object>> data = new ArrayList<java.util.Map<String, Object>>();
    private ArrayList<java.util.Map<String, Object>> data1 = new ArrayList<java.util.Map<String, Object>>();
    private String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        ResourceDb.get_instance().OpenDB(Constants.DATABASE_PATH);


        if (!Utils.isFileExist(Constants.BASEPATH + language + "/")) {
            final NiftyDialogBuilder builder = new NiftyDialogBuilder(MainActivity.this, R.style.dialog_untran);
            Effectstype effect = Effectstype.Slidetop;
            builder.setTypeface(HD_Application.TEXT_TYPE);
            builder.withTitle(getString(R.string.tips))
                    .withMessage(getString(R.string.download_all_res))
                    .withIcon(getResources().getDrawable(R.drawable.app_icon))
                    .withDuration(700)
                    .withEffect(effect)
                    .withButton1Text(getString(R.string.confirm)).setButton1Click(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builder.dismiss();
                    download(HdAppConfig.getAddress() + language + ".zip", Constants.BASEPATH + "temp/" + language + ".zip", Constants.BASEPATH, mHandler1, false);
                }
            }).withButton2Text(getString(R.string.cancel)).setButton2Click(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builder.dismiss();
                }
            });
            builder.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        language = Constants.LANGUAGE;
    }

    @OnClick(R.id.overview)
    public void overview(View view) {
        String overviewPath = Constants.BASEPATH + language + "/0001/0001.html";
        File file = new File(overviewPath);
        if (file.exists()) {
//            Bundle data = new Bundle();
//            data.putString("title", getString(R.string.overview));
//            data.putString("path", overviewPath);
//            openActivity(CommonWeb.class, data);
            PlayInfo info = new PlayInfo();
            info.setPath(Constants.BASEPATH + language + "/0001/0001.mp3");
            info.setInfo(getString(R.string.overview));
            info.setNum("0001");
            Bundle bundle = new Bundle();
            bundle.putSerializable("PlayInfo", info);
            openActivity(AudioPlayer.class, bundle);
        } else {
            showShortToast(R.string.please_download_resource);
            openActivity(Settings.class);
        }
    }

    @OnClick(R.id.map)
    public void map(View view) {
        openActivity(Map.class);
    }

    @OnClick(R.id.digital)
    public void digital(View view) {
        openActivity(Diaital.class);
    }

    @OnClick(R.id.qr)
    public void qr(View view) {
        startActivityForResult(new Intent(MainActivity.this, QrActivity.class), GET_CODE);
    }

    @OnClick(R.id.auto)
    public void auto(View view) {
        openActivity(PlayUnit.class);
    }

    @OnClick(R.id.service)
    public void service(View view) {
        openActivity(Guide.class);
    }


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("PlayInfo", info);
                    openActivity(AudioPlayer.class, bundle);
                    break;
            }
        }
    };

    private Handler mHandler1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    showShortToast(R.string.download_complete);
                    break;
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
        }
        return false;
    }

    private void exitBy2Click() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            showShortToast(R.string.exit_app);
            mExitTime = System.currentTimeMillis();
        } else {
            defaultFinish();
            System.exit(0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == MainActivity.this.RESULT_OK && requestCode == GET_CODE) {

            //String result = String.format("%1$04d", Integer.parseInt(data.getExtras().getString("result")));
            String result = data.getExtras().getString("result");
            cursor = ResourceDb.get_instance().PlayByFileNum(language, result);
            if (cursor != null) {
                cursor.moveToNext();
                info = PlayUtils.setPlayInfo(cursor);
                if (!FileUtils.isFileExist(cursor.getString(2), cursor.getString(1), cursor.getString(3))) {
                    showShortToast(R.string.please_download_resource);
                    openActivity(Settings.class);
                } else {
                    mHandler.sendEmptyMessage(0);
                }
            } else {
                showShortToast(R.string.invalid_number);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ResourceDb.get_instance().CloseDB();
        ResourceDb._instance = null;
    }
}
