package com.sh.museum.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.common.Constants;

import java.io.File;

@ContentView(R.layout.guide)
public class Guide extends BaseActivity {
    @ViewInject(R.id.top_back_btn)
    ImageButton back;
    @ViewInject(R.id.top_title)
    TextView title;
    @ViewInject(R.id.zhinan)
    private RelativeLayout zhinan;
    @ViewInject(R.id.traffic)
    private RelativeLayout traffic;
    @ViewInject(R.id.environment)
    private RelativeLayout environment;
    @ViewInject(R.id.activity)
    private RelativeLayout activity;
    @ViewInject(R.id.setting)
    private RelativeLayout setting;
    private String language = Constants.LANGUAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        title.setText(R.string.service);
    }

    @OnClick(R.id.zhinan)
    public void zhinan(View view) {
        String path = Constants.BASEPATH + language + "/1301/1301.html";
        if (validatPath(path)) {
            Bundle data = new Bundle();
            data.putString("title", getString(R.string.guide_zhinan));
            data.putString("path", path);
            openActivity(CommonWeb.class, data);
        }
    }

    @OnClick(R.id.traffic)
    public void traffic(View view) {
        String path = Constants.BASEPATH + language + "/1302/1302.html";
        if (validatPath(path)) {
            Bundle data = new Bundle();
            data.putString("title", getString(R.string.guide_traffic));
            data.putString("path", path);
            openActivity(CommonWeb.class, data);
        }
    }

    @OnClick(R.id.environment)
    public void environment(View view) {
        String path = Constants.BASEPATH + language + "/1303/1303.html";
        if (validatPath(path)) {
            Bundle data = new Bundle();
            data.putString("title", getString(R.string.guide_environment));
            data.putString("path", path);
            openActivity(CommonWeb.class, data);
        }
    }

    @OnClick(R.id.activity)
    public void activity(View view) {
        String path = Constants.BASEPATH + language + "/1304/1304.html";
        if (validatPath(path)) {
            Bundle data = new Bundle();
            data.putString("title", getString(R.string.guide_activitys));
            data.putString("path", path);
            openActivity(CommonWeb.class, data);
        }
    }

    @OnClick(R.id.setting)
    public void setting(View view) {
        openActivity(Settings.class);
    }

    private boolean validatPath(String path) {
        boolean flag = false;
        File file = new File(path);
        if (file.exists()) {
            flag = true;
        } else {
            showShortToast(R.string.please_download_resource);
            openActivity(Settings.class);
            finish();
        }
        return flag;
    }


    @OnClick(R.id.top_back_btn)
    public void back(View view) {
        defaultFinish();
    }
}
