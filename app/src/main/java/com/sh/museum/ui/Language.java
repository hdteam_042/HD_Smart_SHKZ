package com.sh.museum.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.common.Constants;
import com.sh.museum.utils.HdAppConfig;
import com.sh.museum.utils.Utils;
import com.sh.museum.view.InitializeDialog;

@ContentView(R.layout.language)
public class Language extends BaseActivity {
    @ViewInject(R.id.chinese)
    private Button chinese;
    @ViewInject(R.id.english)
    private Button english;
    @ViewInject(R.id.japanese)
    private Button japanese;
    private InitializeDialog dialog;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.lidroid.xutils.ViewUtils.inject(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getConnectedType(this) == 1) {
            if (getWifiName().contains(Constants.SSID1)||getWifiName().contains(Constants.SSID2)) {
                HdAppConfig.setAddress(Constants.ADDERSS_INNER);
            } else {
                HdAppConfig.setAddress(Constants.ADDERSS_OUTTER);
            }
        } else if (getConnectedType(this) == -1) {
            showNetWorkDialog();
        }
        if (getConnectedType(this) != -1) {

            dialog = new InitializeDialog(Language.this, mHandler, Constants.BASEPATH
                    + "temp/filemanage.zip", HdAppConfig.getAddress() + "database.zip", Constants.BASEPATH + "database/");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.setMessage(getResources().getString(R.string.initialize));
            dialog.show();
        }
    }

    public void showNetWorkDialog() {
        final NiftyDialogBuilder builder = new NiftyDialogBuilder(Language.this, R.style.dialog_untran);
        Effectstype effect = Effectstype.Slidetop;
        builder.withTitle(getString(R.string.network_error))
                .withMessage(getString(R.string.check_network))
                .withIcon(getResources().getDrawable(R.drawable.app_icon))
                .withDuration(700)
                .withEffect(effect)
                .withButton1Text(getString(R.string.confirm)).setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(android.provider.Settings.ACTION_SETTINGS);
                startActivity(in);
                builder.dismiss();
            }
        }).withButton2Text(getString(R.string.cancel)).setButton2Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        builder.show();
    }

    private String getWifiName() {
        String wifiName;
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        wifiName = wifiInfo.getSSID();
        return wifiName;
    }

    @OnClick(R.id.chinese)
    public void chinese(View view) {
        Constants.LANGUAGE = "CHINESE";
        Utils.configLanguage(Language.this, "CHINESE");
        startActivity(new Intent(Language.this, MainActivity.class));
        finish();
    }

    @OnClick(R.id.english)
    public void english(View view) {
        Constants.LANGUAGE = "ENGLISH";
        Utils.configLanguage(Language.this, "ENGLISH");
        startActivity(new Intent(Language.this, MainActivity.class));
        finish();
    }

    @OnClick(R.id.japanese)
    public void japanese(View view) {
        Constants.LANGUAGE = "JAPANESE";
        Utils.configLanguage(Language.this, "JAPANESE");
        startActivity(new Intent(Language.this, MainActivity.class));
        finish();
    }

    private long mExitTime;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
        }
        return false;
    }

    private void exitBy2Click() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            showShortToast(R.string.exit_app);
            mExitTime = System.currentTimeMillis();
        } else {
            defaultFinish();
            System.exit(0);
        }
    }
}
