package com.sh.museum.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.base.BaseActivity;

@ContentView(R.layout.common_web)
public class CommonWeb extends BaseActivity {
    @ViewInject(R.id.top_back_btn)
    ImageButton back;
    @ViewInject(R.id.top_title)
    TextView title;
    @ViewInject(R.id.webview)
    private WebView content;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        getPlayInfo();
        WebSettings webSettings = content.getSettings();
        webSettings.setJavaScriptEnabled(true);
        content.setBackgroundColor(Color.TRANSPARENT);
        content.loadUrl("file:///" + path);

    }

    private void getPlayInfo() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        title.setText(bundle.getString("title"));
        path = bundle.getString("path");
    }

    @OnClick(R.id.top_back_btn)
    public void back(View view) {
        defaultFinish();
    }

}
