package com.sh.museum.ui;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.adapter.ResourceAdapter;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.common.Constants;
import com.sh.museum.db.ResourceDb;
import com.sh.museum.entities.PlayInfo;
import com.sh.museum.utils.FileUtils;
import com.sh.museum.utils.PlayUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by baishiwei on 2015/1/23.
 */
@ContentView(R.layout.play_list)
public class PlayList extends BaseActivity {
    @ViewInject(R.id.top_back_btn)
    ImageButton back;
    @ViewInject(R.id.top_title)
    TextView title;
    @ViewInject(R.id.list)
    private ListView list;
    private String type;
    private Cursor cursor;
    private ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
    private ResourceAdapter adapter;
    private PlayInfo info;
    private String language = Constants.LANGUAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        getInfo();
        resourceList();
        adapter = new ResourceAdapter(this, data, mHandler);
        list.setAdapter(adapter);
    }

    private void getInfo() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        type = bundle.getString("type");
        title.setText(bundle.getString("title"));
        title.setSelected(true);
    }

    private void resourceList() {
        if (data != null) {
            data.clear();
        }
        cursor = ResourceDb.get_instance().resourceList(language, type);
        Map<String, Object> item;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                item = new HashMap<String, Object>();
                imageItem(item, cursor.getString(1));
                item.put("title", cursor.getString(4));
                item.put("Num", cursor.getString(1));
                item.put("Path", cursor.getString(2));
                item.put("Type", cursor.getString(3));
                item.put("sort",cursor.getString(7));
                data.add(item);
            }
        }
        Collections.sort(data,new ComparatorMap());
    }

    class ComparatorMap implements Comparator<Map<String,Object>>{

        @Override
        public int compare(Map<String, Object> m1, Map<String, Object> m2) {
            int flag=(Integer.valueOf((String) m1.get("sort"))).compareTo(Integer.valueOf((String) m2.get("sort")));
            return flag;
        }
    }

    private void imageItem(Map<String, Object> item, String mCurID) {
        String temp_path = Constants.BASEPATH + language + "/" + mCurID + "/" + mCurID + "_icon.png";
        File imageFile = new File(temp_path);
        if (imageFile.exists()) {
            item.put("Image", BitmapFactory.decodeFile(temp_path));
        } else {
            item.put("Image", BitmapFactory.decodeResource(getResources(),R.drawable.default_image));
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("PlayInfo", info);
                    openActivity(AudioPlayer.class, bundle);
                    break;
                case 1:
                    Bundle pBundle = msg.getData();
                    int position = pBundle.getInt("position");
                    play(data.get(position));
                    break;
            }
        }
    };

    private void play(Map<String, Object> data) {
        info = PlayUtils.setPlayInfo(data);
        if (!FileUtils.isFileExist(data.get("Path").toString(), data.get("Num").toString(), data.get("Type").toString())) {
            showShortToast(R.string.please_download_resource);
            openActivity(Settings.class);
        } else {
            mHandler.sendEmptyMessage(0);
        }
    }

    @OnClick(R.id.top_back_btn)
    public void back(View view) {
        defaultFinish();
    }
}
