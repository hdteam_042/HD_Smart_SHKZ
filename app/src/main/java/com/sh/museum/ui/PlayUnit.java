package com.sh.museum.ui;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.sh.museum.R;
import com.sh.museum.adapter.UnitAdapter;
import com.sh.museum.base.BaseActivity;
import com.sh.museum.common.Constants;
import com.sh.museum.db.ResourceDb;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@ContentView(R.layout.play_unit)
public class PlayUnit extends BaseActivity {
    @ViewInject(R.id.top_back_btn)
    private ImageButton back;
    @ViewInject(R.id.top_title)
    private TextView title;
    @ViewInject(R.id.list)
    private ListView list;
    private UnitAdapter adapter;
    private ArrayList<java.util.Map<String, Object>> data = new ArrayList<java.util.Map<String, Object>>();
    private ArrayList<Integer> imageIDs = new ArrayList<>();
    private String language = Constants.LANGUAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        title.setText(R.string.list);
       // imageIDs.add(R.drawable.unit_1);
        imageIDs.add(R.drawable.unit_3);
        imageIDs.add(R.drawable.unit_2);
        getUnitList();
        data.remove(0);
        adapter = new UnitAdapter(PlayUnit.this, data,imageIDs);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle para = new Bundle();
                para.putString("title", (String) data.get(position).get("title"));
                para.putString("type", (String) data.get(position).get("type"));
                openActivity(PlayList.class, para);
            }
        });
    }

    private void getUnitList() {
        Cursor listCursor = ResourceDb.get_instance().getUnitList(language + "UTIL");
        Map<String, Object> item;
        if (listCursor != null) {
            while (listCursor.moveToNext()) {
                item = new HashMap<String, Object>();
                imageItem(item, listCursor.getString(1));
                item.put("title", listCursor.getString(0));
                item.put("type", listCursor.getString(1));
                data.add(item);
            }
        }
    }

    private void imageItem(Map<String, Object> item, String mCurID) {
        String temp_path = Constants.BASEPATH + "base/" + mCurID + "_unit.png";
        File imageFile = new File(temp_path);
        if (imageFile.exists()) {
            item.put("Image", BitmapFactory.decodeFile(temp_path));
        } else {
            item.put("Image", BitmapFactory.decodeResource(getResources(),R.drawable.default_image));
        }
    }

    @OnClick(R.id.top_back_btn)
    public void back(View view) {
        defaultFinish();
    }
}
