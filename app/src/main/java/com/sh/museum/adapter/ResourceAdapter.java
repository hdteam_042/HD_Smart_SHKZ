package com.sh.museum.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.sh.museum.R;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by baishiwei on 2015/1/23.
 */
public class ResourceAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Map<String, Object>> data;
    private Handler mHandler;
    private BitmapUtils bitmapUtils;

    public ResourceAdapter(Context context, ArrayList<Map<String, Object>> data, Handler mHandler) {
        this.context = context;
        this.data = data;
        this.mHandler = mHandler;
        bitmapUtils = new BitmapUtils(context);
        bitmapUtils.clearCache();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data == null || data.isEmpty() ? 0 : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.resource_listitem, null);
            holder.layout = (RelativeLayout) convertView.findViewById(R.id.layout);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String title = (String) data.get(position).get("title");
        if (title.contains("：")){
            String[] strings = title.split("：");
            title=strings[0]+":\n"+strings[1];
        }
        if (title.contains(":")){
            String[] strings = title.split(":");
            title=strings[0]+":\n"+strings[1];
        }
        holder.title.setText(title);
        // bitmapUtils.display(holder.image, (String) data.get(position).get("Image"));
        holder.image.setImageBitmap((Bitmap) data.get(position).get("Image"));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message msg = mHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                msg.setData(bundle);
                msg.what = 1;
                mHandler.sendMessage(msg);
            }
        });
        return convertView;
    }

    private class ViewHolder {
        RelativeLayout layout;
        TextView title;
        ImageView image;
    }
}
