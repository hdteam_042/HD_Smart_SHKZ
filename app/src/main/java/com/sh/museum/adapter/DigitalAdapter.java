package com.sh.museum.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.sh.museum.R;

import java.util.ArrayList;
import java.util.Map;


public class DigitalAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Map<String, Object>> data;
    private BitmapUtils bitmapUtils;

    public DigitalAdapter(Context context, ArrayList<Map<String, Object>> data) {
        this.context = context;
        this.data = data;
        bitmapUtils = new BitmapUtils(context);
        bitmapUtils.clearCache();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data == null || data.isEmpty() ? 0 : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.digital_listitem, null);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText((String) data.get(position).get("title"));
        // bitmapUtils.display(holder.image, (String) data.get(position).get("Image"));
        holder.image.setImageBitmap((Bitmap) data.get(position).get("Image"));
        return convertView;
    }

    private class ViewHolder {
        TextView title;
        ImageView image;
    }
}
