package com.sh.museum.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh.museum.R;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by baishiwei on 2015/1/23.
 */
public class UnitAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Map<String, Object>> data;
    private ArrayList<Integer> imageIDs;

    public UnitAdapter(Context context, ArrayList<Map<String, Object>> data, ArrayList<Integer> imageIDs) {
        this.context = context;
        this.data = data;
        this.imageIDs = imageIDs;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data == null || data.isEmpty() ? 0 : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.unit_listitem, null);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText((String) data.get(position).get("title"));
       // holder.image.setImageBitmap((Bitmap) data.get(position).get("Image"));
        holder.image.setImageResource(imageIDs.get(position));
        return convertView;
    }

    private class ViewHolder {
        TextView title;
        ImageView image;
    }
}
