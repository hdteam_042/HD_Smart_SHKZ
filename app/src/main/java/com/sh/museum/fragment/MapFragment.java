package com.sh.museum.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lidroid.xutils.BitmapUtils;
import com.qozix.tileview.TileView;
import com.sh.museum.common.Constants;
import com.sh.museum.entities.MapBase;

import java.io.Serializable;

public class MapFragment extends Fragment {
    private TileView tileView;
    private MapBase mapBase;
    private int floor = 2;
    private IntentFilter filter;
    private ImageView road;
    private double temp_x;
    private double temp_y;
    private int count;
    private BitmapFactory.Options options;
    private Bitmap roadBm;
    private String language;
    private String map_path;
    private boolean bo =false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapBase = (MapBase) getArguments().getSerializable("baseinfo");
        floor = getArguments().getInt("floor");
        temp_x = getArguments().getDouble("x");
        temp_y = getArguments().getDouble("y");
        language = getArguments().getString("language");
        bo = getArguments().getBoolean("b");
        if (bo) {
            map_path = Constants.BASEPATH + language + "/map1/";
        } else {
            map_path = Constants.BASEPATH + language + "/map/";
        }

    }

    public static MapFragment newInstance(Serializable arg1, int floor, String language) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("baseinfo", arg1);
        bundle.putInt("floor", floor);
        bundle.putString("language", language);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MapFragment newInstance(Serializable arg1, int floor, String language, boolean b) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("baseinfo", arg1);
        bundle.putInt("floor", floor);
        bundle.putString("language", language);
        bundle.putBoolean("b", b);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        road = new ImageView(getActivity());
        options = new BitmapFactory.Options();
        options.inSampleSize = 4;//图片压缩比
        tileView = new TileView(getActivity());
        tileView.setTransitionsEnabled(false);// by disabling transitions, we won't see a flicker of background color when moving between tile sets
        tileView.setSize((int) mapBase.getWidth(), (int) mapBase.getHeight());//size of original image at 100% scale
        // detail levels
        tileView.addDetailLevel(1.000f, map_path + floor + "/1000/%col%_%row%.jpg", map_path + floor + ".png");
        tileView.addDetailLevel(0.500f, map_path + floor + "/500/%col%_%row%.jpg", map_path + floor + ".png");
        tileView.addDetailLevel(0.250f, map_path + floor + "/250/%col%_%row%.jpg", map_path + floor + ".png");
        tileView.addDetailLevel(0.125f, map_path + floor + "/125/%col%_%row%.jpg", map_path + floor + ".png");
        ImageView downSample = new ImageView(getActivity());
        new BitmapUtils(getActivity()).display(downSample, map_path + floor + "/img.jpg");
        //downSample.setImageBitmap(BitmapUtils.loadBitmapFromFile(map_path + floor + "/img.jpg"));
        tileView.addView(downSample, 0);
        tileView.setScale(0);
        tileView.setScaleLimits(0, (int) mapBase.getScale());
        tileView.setMarkerAnchorPoints(-0.5f, -0.5f);
        frameTo((int) mapBase.getWidth() / 2, (int) mapBase.getHeight() / 2);
        initIntent();
        return tileView;
    }

    @Override
    public void onPause() {
        super.onPause();
        tileView.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        tileView.postDelayed(new Runnable() {
            @Override
            public void run() {
                tileView.refresh();
            }
        }, 300);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tileView.destroy();
        tileView = null;
        getActivity().unregisterReceiver(recevier);
    }

    /**
     * This is a convenience method to moveToAndCenter after layout (which won't happen if called directly in onCreate
     * see https://github.com/moagrius/TileView/wiki/FAQ
     */
    public void frameTo(final double x, final double y) {
        tileView.post(new Runnable() {
            @Override
            public void run() {
                tileView.moveToAndCenter(x, y);
            }
        });
    }

    private void initIntent() {
        filter = new IntentFilter();
        filter.addAction(Constants.MAP_NAVIGATION);
        getActivity().registerReceiver(recevier, filter);
    }

    private BroadcastReceiver recevier = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.MAP_NAVIGATION)) {
                if (count % 2 == 0) {
                    if (roadBm == null) {
                        roadBm = BitmapFactory.decodeFile(map_path + "/route" + floor + ".png", options);
                        road.setImageBitmap(roadBm);
                    }
                    tileView.addView(road, 4, new ViewGroup.LayoutParams(tileView.getBaseWidth(), tileView.getBaseHeight()));
                } else {
                    tileView.removeViewAt(4);
                }
                count++;
            }
        }
    };

}
