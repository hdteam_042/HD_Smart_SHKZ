package com.qozix.tileview.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapDecoderAssets implements BitmapDecoder {

    private static final BitmapFactory.Options OPTIONS = new BitmapFactory.Options();

    static {
        OPTIONS.inPreferredConfig = Bitmap.Config.RGB_565;
    }

    @Override
    public Bitmap decode(String fileName, Context context) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;//图片压缩比
        Bitmap bm = BitmapFactory.decodeFile(fileName, options);
        if (bm != null) {
            try {
                return bm;
            } catch (OutOfMemoryError oom) {
                // oom - you can try sleeping (this method won't be called in the UI thread) or try again (or give up)
            } catch (Exception e) {
                // unknown error decoding bitmap
            }
        }

//		AssetManager assets = context.getAssets();
//		try {
//			InputStream input = assets.open( fileName );
//			if ( input != null ) {
//				try {
//					return BitmapFactory.decodeStream( input, null, OPTIONS );
//				} catch ( OutOfMemoryError oom ) {
//					// oom - you can try sleeping (this method won't be called in the UI thread) or try again (or give up)
//				} catch ( Exception e ) {
//					// unknown error decoding bitmap
//				}
//			}
//		} catch ( IOException io ) {
//			// io error - probably can't find the file
//		} catch ( Exception e ) {
//			// unknown error opening the asset
//		}
        return null;
    }


}
